const mongoose = require('mongoose');


const CartSchema = new mongoose.Schema(
    {
        userId: {
            type:String,
            required: true
        },
        products : [
            {
                productId: {
                    type:String,
                },
                quantity: {
                    type:Number,
                    min: 1,
                },
                totalPrice: {
                    type:Number
                }
            }
        ],
        subTotal: {
            type:Number
        }
       
    })

module.exports = mongoose.model("Cart", CartSchema);