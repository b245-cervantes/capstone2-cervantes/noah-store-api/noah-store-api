const mongoose = require('mongoose');
const Order = require('../Models/orderSchema');
const Product = require('../Models/productSchema');
const auth = require('../src/auth')
const Cart = require('../Models/cartSchema')

// ADD ITEM!!
module.exports.addItem = (req,res) => {
    const userData = auth.decode(req.headers.authorization)
    let input = req.body
     
    if(userData.isAdmin){
        res.send("Please login as a user!")
    } else {
            Cart.findOne({userId: userData._id})
            .then(result => {
              if(result){
                return res.send("You already added items in your cart!")
              } else {
                Product.findOne({productId: input.productId})
                .then(result => {
               let addedItem = new Cart ({
                   userId: userData._id,
                   products: [{
                       productId:input.productId,
                       quantity:input.quantity,
                       totalPrice: result.price * input.quantity
                   }],
                   subTotal:result.price * input.quantity
               })
                 addedItem.save()
                 .then(result => {
                   const {_id,__v, ...others} = result._doc
                   return res.send({...others})
                 })
                 .catch(error => {
                   res.send(false)
                 })
               })
              }
            })
            .catch(error => {
              res.send(false)
            })
}}


// INSERT MORE OR ADD ITEMS
module.exports.insertItem = async (req,res) =>{
    const userData = auth.decode(req.headers.authorization)
    let input = req.body
    if(userData.isAdmin){
        res.send("Please login as a user!")
    } else {
       await Cart.findOne({userId: userData._id})
       .then(result => {
        if(result){
          Product.findOne({productId: input.productId})
            .then(data => {
             result.products.push({productId:input.productId, quantity:input.quantity, totalPrice: data.price * input.quantity})
             result.save()
              let total = result.products.map(add => add.totalPrice);
              let total2 = total.reduce((a,b)=> (a+b));
              let newSubtotal = {
                subTotal: total2
              }
               Cart.findOneAndUpdate({userId: userData._id}, {$set: newSubtotal}, {new:true})
              .then(result => {
                 return res.send(result)
              })
              
            })
            .catch(error => {
             res.send(false)
            })
        } else{
          return res.send("please add item in cart!")
        }
            
       })

    }
}

//Checkout cart!!
module.exports.checkOut = async(req,res) => {
    const userData = auth.decode(req.headers.authorization)
    let input = req.body
    if(userData.isAdmin){
        res.send("Please login as a user!")
    } else {
      await Cart.findOne({userId: userData._id})
      .then(result => {
          if(result === null){
            res.send("The user id is not existing, cart empty!")
          } else {
           
            let newOrder = new Order ({
                userId: userData._id,
                products:[{...result.products}],
                subTotal: result.subTotal
            })
            newOrder.save()
            res.send(emptycart(userData._id))
          }
      })
      .catch(error =>{
        res.send(false)
      })
    }
}

//UPDATE CART ITEMS

module.exports.updateCart = (req,res) => {
   const userData = auth.decode(req.headers.authorization)
   let input = req.body
   if(userData.isAdmin){
      res.send("PLEASE LOGIN AS A USERS")
   } else{
       Cart.findOne({userId:userData._id})
       .then(result => {
         for(let i = 0; i < result.products.length; i++){
               result.products.pop()
         }
         result.save()
         let total = result.products.map(add => add.totalPrice);
         let total2 = total.reduce((a,b)=> (a+b));
         let newSubtotal = {
          subTotal: total2
       }
       Cart.findOneAndUpdate({userId: userData._id}, {$set: newSubtotal}, {new:true})
       .then(result => {
          return res.send(result)
       })
         
       })
       .catch(error =>{
        
        return res.send(false)
       })

   }
}
//My Cart 
module.exports.myCart =(req,res) =>{
  const userData = auth.decode(req.headers.authorization)
   let input = req.body
   if(userData.isAdmin){
      res.send("PLEASE LOGIN AS A USERS")
   } else{
       Cart.find({userId: userData._id})
       .then(result => {
         if(result.length == 0 || result.products.length == 0){
           return res.send("Please add some item in your cart!")
         } else {
          return res.send(result)
         }
       })
       .catch(error =>{
        res.send(false);
        
   })
}
}
 ///function for deleting data!
 function emptycart(userIds) {
       Cart.findOneAndDelete({userId: userIds})
       .then(result => {
         return true
       })
       .catch(error => {
        return false
       })
 }