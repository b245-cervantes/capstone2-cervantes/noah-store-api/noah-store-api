const mongoose = require('mongoose');
const Product = require('../Models/productSchema');
const auth = require('../src/auth');

//creating product or adding product
module.exports.createProduct = (req,res) =>{
    const userData = auth.decode(req.headers.authorization)
    let input = req.body;
    if(!userData.isAdmin){
        res.send("You are not allowed to do that!")
    } else {
        const newProduct = new Product({
            name:input.name,
            description:input.description,
            img:input.img,
            size:input.size,
            color:input.color,
            price:input.price,
            category: input.category
        })
        newProduct.save()
        .then(result =>{
            res.send(result)
        })
        .catch(error => {
            res.send(false)
        })
    }
}

//Upadting product admin only
module.exports.updateProduct = async (req,res) => {
    const userData = auth.decode(req.headers.authorization);
    let input = req.body;
    if(!userData.isAdmin){
        res.send("You are not allowed to do that!!")
    } else {
        let updatedProduct = {
            name:input.name,
            description:input.description,
            img:input.img,
            size:input.size,
            color:input.color,
            price:input.price,
            category: input.category
        }
        await Product.findByIdAndUpdate(req.params._productId, {$set: updatedProduct}, {new:true})
        .then(result => {
            res.send(result)
        })
        .catch(error =>{
            res.send(false)
        })
    }
}

//Retreive all products with query
module.exports.getAll = async (req,res) =>{
    
    let input = req.body
     try{   
        const qeuryNew = req.query.new;
        const querycategory = req.query.category;
         let product;
        
        if(qeuryNew){
            product = await Product.find({isActive:true}).sort({createdAt: -1}).limit(5)
        } else if(querycategory){
            product = await Product.find({isActive:true, category:{
                $in:[querycategory],
            }})
        }else{
           product = await Product.find({isActive:true})
        }
          return res.send(product)
        } catch(error){
          return res.send(false)
        }
}

//Retrieve single product
module.exports.getProduct = (req,res) => {
    let productId = req.params.productId;

    Product.findById(productId)
    .then(result => {
        return res.send(result)
    })
    .catch(error => {
        return res.send(false)
    })
}

//Archiving product or deleting product
module.exports.archiveProduct = (req,res) =>{
    let productId = req.params.productId;
    let input = req.body;
    const userData = auth.decode(req.headers.authorization)

    if(!userData.isAdmin){
        res.send("You are not allowed to do that!!")
    } else{
        let archived = {
            isActive:input.isActive
        }
        Product.findByIdAndUpdate(productId, {$set: archived}, {new: true})
        .then(result => {
            return res.send(result)
        })
        .catch(error => {
            return res.send(false)
        })
    }
}