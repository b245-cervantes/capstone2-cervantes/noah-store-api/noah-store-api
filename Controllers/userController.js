const mongoose = require('mongoose');
const Users = require('../Models/userSchema');
const bcrypt = require('bcrypt');
const auth = require('../src/auth');


//User Registration
module.exports.userReg = (req,res) =>{
    let input = req.body;

    Users.findOne({email:input.email})
    .then(result => {
        if(result !== null){
            return res.send("The email address is already exists!")
        }else {
            let newUser = new Users({
                username: input.username,
                email: input.email,
                password:bcrypt.hashSync(input.password, 10),
            })
            newUser.save()
            .then(save => {
                return res.send("Thank you for Registering! Enjoy Shopping!");
            })
            .catch(error => {
                return res.send(false)
            })
        }
    })
    .catch(error =>{
        res.send(false)
    })
}
//END OF USER REGISTRATION


//LOGIN USERS
module.exports.authUser = (req,res) => {
    let input = req.body;

    Users.findOne({email:input.email})
    .then(result =>{
        if(result === null){
            return res.send("Email is not yet registered, Please click the registration button!");
        } else {
           const isPasswordCorrect = bcrypt.compareSync(input.password, result.password);
           if(isPasswordCorrect){
            const {password, ...others} = result._doc;
            return res.send({...others, auth:auth.createAccessToken(result)})
           }else{
            res.send("Incorrect password! please try again!")
           }
        }
    })
    .catch(error => {
        return res.send(false)
    })
}



//get all users
module.exports.getAll = async(req,res) => {
    
    const userData = auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
    return res.send("You are not allowed to do that!")
         } 
         else  {
            try{
               
                const query = req.query.new;
                const users = query
                 ? await Users.find().sort({_id: -1}).limit(5)
                 : await Users.find();
                 return res.send(users)
            }catch (error){
               return res.send(false)
            }
        
            }
}




//UPDATE USER DATA
module.exports.updateUser = (req,res) => {
    let input = req.body;
   const userData = auth.decode(req.headers.authorization)
    if(input.password){
        input.password =bcrypt.hashSync(input.password, 10)
    }
    let newData = {
        username:input.username,
        email:input.email,
        password:input.password
    }
    Users.findByIdAndUpdate(userData._id, {$set: newData}, {new:true})
    .then(result => {
        const {password, ...others} = result._doc
        return res.send({...others});
    })
    .catch(error => {
        return res.send(false)
    })
}


//delete user
module.exports.deleteUser = (req,res) => {
    let input = req.params._id
    const userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
    Users.findByIdAndDelete(input)
    .then(result =>{
        res.send("The user has been delete!")
    })
    .catch(error =>{
        return res.send(false)
    })
    }else{
        res.send("login as admin please!")
    }
}


//GET single users
module.exports.getUser = async(req,res) => {
    let userId = req.params.userId;
    const userData = auth.decode(req.headers.authorization)
       if(!userData.isAdmin){
        return res.send("You are not allowed to do that!")
       } else {
         await Users.findById(userId)
         .then(result => {
            if(result !== null){
                const {password, ...others} = result._doc
                return res.send({...others})
            }else {
               
               return res.send("INVALID user ID!")
            }
         })
       }
}

//Get Users Statitics per month
module.exports.getUsersStat = async(req,res) => {
    const userData = auth.decode(req.headers.authorization);
    const date = new Date()
    const lastYear = new Date(date.setFullYear(date.getFullYear()-1))
    if(!userData.isAdmin){
        return res.send("You are not allowed to do that!")
    }else {
    try {
          const data = await Users.aggregate([
            {
                $match: {
                    createdAt: {
                        $gte: lastYear
                    }
                }
            },
            {
                $project: {
                    month: {
                        $month: "$createdAt"
                    }
                }
            },
            {
                $group: {
                    _id:"$month",
                    total:{$sum: 1}
                }
            }
          ])
          return res.send(data)
    }catch(error){
        res.send(false)
    }
}
}



//SET USERS AS ADMIN
module.exports.setUsersAdmin = async(req,res) => {
    const userData = auth.decode(req.headers.authorization);
    let input = req.body
    let userId = req.params.userId;
    if(!userData.isAdmin){
        res.send("You are not allowed to do that!")
    } else {
        let newAdmin = {
           isAdmin: input.isAdmin
        }
        await Users.findByIdAndUpdate(userId, {$set: newAdmin}, {new:true})
        .then(result => {
            const{password, ...others} = result._doc
            res.send({...others})
        })
        .catch(error => {
            res.send(false)
        })

    }
}

