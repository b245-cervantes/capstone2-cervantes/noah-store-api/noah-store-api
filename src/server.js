const express = require('express');
const app = express();
const mongodb = require('./mongodb');
const userRoute = require('../Routes/userRoute');
const productRoute = require('../Routes/productRoute');
const orderRoute = require('../Routes/orderRoute');
const port = 3005;
const cartRoute = require('../Routes/cartRoute')






//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//route for user
app.use('/api/users', userRoute);

//router for product
app.use('/api/products', productRoute);

//router for order
app.use('/api/order', orderRoute);

//router for cart
app.use('/api/cart', cartRoute)


app.listen(port, ()=>{
    console.log(`The server is running at port ${port}`)
})