const mongoose = require('mongoose');
const express = require('express');
const mongodb = express.mongodb;
const dotenv = require('dotenv');

dotenv.config();

mongoose.set("strictQuery", true);
mongoose.connect(process.env.MONGODB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "error"));
db.once("open", ()=> {
    console.log("We are connected to cloud server!!")
})

module.exports = mongodb