const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const cartController = require('../Controllers/cartController');
const auth = require('../src/auth');

// add item
router.post('/', auth.verify, cartController.addItem);

//insert item
router.post('/insert', auth.verify, cartController.insertItem)

//Checkout items
router.post('/checkout', auth.verify, cartController.checkOut)

//MyCART

router.get('/myCart', auth.verify, cartController.myCart)

//Update Cart items
router.patch('/removeCart', auth.verify, cartController.updateCart)

module.exports = router;