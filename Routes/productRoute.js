const express = require('express')
const mongoose = require('mongoose')
const Product = require('../Models/productSchema')
const router = express.Router();
const auth = require('../src/auth')
const productController = require('../Controllers/productController')


//route for creating product
router.post('/create', auth.verify, productController.createProduct);

//route for retreiving all the products
router.get('/',productController.getAll)

//route for archiving products or delete
router.patch('/archived/:productId', auth.verify, productController.archiveProduct)

//route for retreiving single product
router.get('/:productId', productController.getProduct)

//route for updating product
router.put('/update/:_productId', auth.verify, productController.updateProduct)








module.exports = router