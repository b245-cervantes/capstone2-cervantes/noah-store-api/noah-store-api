const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Users = require('../Models/userSchema');
const userController = require('../Controllers/userController');
const auth = require('../src/auth')




//route for retrieve all user
router.get('/', auth.verify, userController.getAll)

//route ofr registering
router.post('/register', userController.userReg)

//route login for authenticate user
router.post('/login',userController.authUser)

//route for users stats per month
router.get('/stats', auth.verify, userController.getUsersStat)

//route for updating as admin by admin only
router.patch('/:userId', auth.verify, userController.setUsersAdmin)

//route update for users info
router.put('/:id', auth.verify, userController.updateUser)

//route for delete user
router.delete('/:id', auth.verify, userController.deleteUser)

//retrieve single user
router.get('/find/:userId', auth.verify, userController.getUser)




module.exports = router;
