const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const orderController = require('../Controllers/orderController')
const auth = require('../src/auth')

//Route for creating or buy me now ORDER
router.post('/',auth.verify,orderController.createOrder)

//Route for retrieving all Orders ADMIN ONLY
router.get('/allorder',auth.verify,orderController.getAllOrder)

//Route for retrieving auth users order non-admin only
router.get('/userorder', auth.verify, orderController.getUserOrder)





module.exports = router